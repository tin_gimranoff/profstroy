#coding: utf-8
class SiteController < ApplicationController

	before_filter :construct

	def index
		@paetitle = 'ГЛАВНАЯ'
	end

	def news
		if params[:id]
			news = News.find(params[:id])
			@content = news.content
			@pagetitle = news.name
			render :textpage
		else
			@news = News.paginate(:page => params[:page], :per_page => 6)
			@pagetitle = 'НОВОСТИ'
			render :news
		end
	end

	def articles
		if params[:id]
			article = Articles.find(params[:id])
			@content = article.content
			@pagetitle = article.name
			render :textpage
		else
			@news = Articles.paginate(:page => params[:page], :per_page => 6)
			@pagetitle = 'СТАТЬИ'
			render :news
		end
	end

	def textpage
		page = Textpage.where(alias: params[:slug])
		if page
			@pagetitle = page[0].title
			@content = page[0].content
			render :textpage
		else
			render '404'
		end
	end

	def works
		@pagetitle = 'НАШИ РАБОТЫ'
		if params[:alias]
			category = Category.where(alias: params[:alias])[0]
			@albums = Album.where(categorie_id: category.id).paginate(:page => params[:page], :per_page => 4)
		else
			@albums = Album.paginate(:page => params[:page], :per_page => 4)
		end
	end

	def sitemap
		respond_to do |format|
			format.html {
				@urls = Array.new
				@pagetitle = 'КАРТА САЙТА'
				url = '/'
				page_info = Seo.where(url: url)
				if !page_info[0]
					page_title = url
				else
					page_title = page_info[0].title
				end
				@urls.push({title: page_title, url: url, childs: Hash.new})

				url = '/news'
				page_info = Seo.where(url: url)
				if !page_info[0]
					page_title = url
				else
					page_title = page_info[0].title
				end
				@urls.push({title: page_title, url: url, childs: Array.new})

				news = News.all
				news.each do |n|
					page_info = Seo.where(url: url+'/'+n.id.to_s)
					if !page_info[0]
						page_title = url
					else
						page_title = page_info[0].title
					end
					@urls[1][:childs].push({title: page_title, url: url+'/'+n.id.to_s, childs: Array.new})
				end

				url = '/works'
				page_info = Seo.where(url: url)
				if !page_info[0]
					page_title = url
				else
					page_title = page_info[0].title
				end
				@urls.push({title: page_title, url: url, childs: Array.new})

				url = '/articles'
				page_info = Seo.where(url: url)
				if !page_info[0]
					page_title = url
				else
					page_title = page_info[0].title
				end
				@urls.push({title: page_title, url: url, childs: Array.new})

				articles = Articles.all
				articles.each do |n|
					page_info = Seo.where(url: url+'/'+n.id.to_s)
					if !page_info[0]
						page_title = url
					else
						page_title = page_info[0].title
					end
					@urls[3][:childs].push({title: page_title, url: url+'/'+n.id.to_s, childs: Array.new})
				end

				texages = Textpage.all
				texages.each do |n|
					page_info = Seo.where(url: '/'+n.alias.to_s)
					if !page_info[0]
						page_title = '/'+n.alias.to_s
					else
						page_title = page_info[0].title
					end
					@urls.push({title: page_title, url: '/'+n.alias.to_s, childs: Array.new})
				end

			}

			format.xml {
				headers['Cntent-Type'] = "aplication/xml"
				render :layout => false
			}
		end
	end

	private
		def construct

			page_info = Seo.where(url: request.path)[0]
			if !page_info.blank?
				@title = page_info.title
				@keywords = page_info.keywords
				@description = page_info.description
			end

			@slider = Slider.all
			@news_widget = News.limit(3)

			if params[:getcall]
	          @getcall = Getcall.new(params[:getcall])
	          if @getcall.valid?
	             SiteMailer.getcall_email(@getcall.name, @getcall.topic, @getcall.phone, @getcall.email, @getcall.message).deliver
	          end
	        else
	           @getcall = Getcall.new
	        end
		end
end