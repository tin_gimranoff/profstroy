#coding: utf-8
ActiveAdmin.register Textpage do

  menu label: "Текстовые страницы"
  permit_params :title, :alias, :content
  index do
    selectable_column
    id_column
    column :title
    column :alias
    actions
  end
  
  form(:html => {:multipart => true}) do |f|
      f.inputs "Новая текстовая страница" do
        f.input :title
        f.input :alias
        f.input :content, :as => :ckeditor
      end
      f.actions
  end
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
