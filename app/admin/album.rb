#coding: utf-8
ActiveAdmin.register Album do

  menu label: "Работы"
  permit_params :image, :title, :intro, :categorie_id
  index do
    selectable_column
    id_column
    column :image do |album|
      image_tag(album.image.url(:thumb)) 
    end
    column :title
    column :categorie_id do |c|
      if !c.categorie_id.blank? and !Category.where(id: c.categorie_id)[0].blank?
        Category.find(c.categorie_id).name
      end
    end
    actions
  end
  
  form(:html => {:multipart => true}) do |f|
      f.inputs "Новая работа" do
        f.input :title
        f.input :intro, :as => :ckeditor
        f.input :image, :required => false, :as => :file
        f.input :categorie_id, as: :select, collection: Category.all
      end
      f.actions
  end

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
