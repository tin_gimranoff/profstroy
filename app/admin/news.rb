#coding: utf-8
ActiveAdmin.register News do

  menu label: "Новости"
  permit_params :image, :name, :intro, :content
  index do
    selectable_column
    id_column
    column :name
    column :intro
    actions
  end
  
  form(:html => {:multipart => true}) do |f|
      f.inputs "Новая новость" do
        f.input :name
        f.input :intro, :as => :ckeditor
        f.input :content, :as => :ckeditor
        f.input :image, :required => false, :as => :file
      end
      f.actions
  end
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
