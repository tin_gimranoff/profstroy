#coding: utf-8
ActiveAdmin.register Photo do

  menu label: "Фото работ"
  permit_params :image, :title, :intro, :album_id
  index do
    selectable_column
    id_column
    column :image do |album|
      image_tag(album.image.url(:thumb)) 
    end
    column :intro do |album|
        album.intro.html_safe
    end
    column :album_id do |r|
      Album.find(r.album_id).title
    end
    actions
  end
  
  form(:html => {:multipart => true}) do |f|
      f.inputs "Новое фото" do
        f.input :intro, :as => :ckeditor
        f.input :image, :required => false, :as => :file
        f.input :album_id, as: :select, collection: Album.all
      end
      f.actions
  end
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
