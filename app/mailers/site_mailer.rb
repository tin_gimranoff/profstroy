#coding: utf-8
class SiteMailer < ActionMailer::Base
  default from: "robot@stroy-stroy.com"

  def getcall_email(name, topic, phone, email, message)
  	@name = name
  	@topic = topic
  	@phone = phone
  	@email = email
  	@message = message
  	mail(to: 'ps.center@mail.ru', subject: 'Заяввка на обратный звонок') do |format|
      format.text { render "getcall_email" }
    end
  end
end
