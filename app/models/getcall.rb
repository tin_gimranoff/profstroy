class Getcall
	include ActiveModel::Conversion
    include ActiveModel::AttributeMethods
    include ActiveModel::Validations
    extend  ActiveModel::Naming
    extend  ActiveModel::Translation

  attr_accessor :name, :topic, :phone, :email, :message

  validates :name, :topic, :phone, presence: true
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  validates_format_of :phone, :with => /\A[0-9+\-\ \(\)]*\z/i

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def persisted?
    false
  end
end