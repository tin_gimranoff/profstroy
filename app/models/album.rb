class Album < ActiveRecord::Base

	has_many :photo
	belongs_to :category

	has_attached_file :image, 
		:styles => { :thumb => "356x258>" }, 
		:path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", 
		:url => "/:class/:attachment/:id/:style_:basename.:extension"

	validates :title, :intro,  presence: true

	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
