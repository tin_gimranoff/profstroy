class Photo < ActiveRecord::Base

	belongs_to :album

	has_attached_file :image, 
		:styles => { :thumb => "90x90>" }, 
		:path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", 
		:url => "/:class/:attachment/:id/:style_:basename.:extension"

	validates :intro, presence: true

	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
