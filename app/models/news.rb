class News < ActiveRecord::Base
	has_attached_file :image, 
		:styles => { :thumb => "198x144>" }, 
		:path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", 
		:url => "/:class/:attachment/:id/:style_:basename.:extension"

	validates :name, :intro, :content,  presence: true

	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
