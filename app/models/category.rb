class Category < ActiveRecord::Base
	has_many :album

	validates :alias, :name, presence: true
end
