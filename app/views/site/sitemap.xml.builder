xml.instruct! :xml, :version=>"1.0"
xml.urlset(:xmlns => "http://www.google.com/schemas/sitemap/0.84") {
	xml.url {
		xml.loc('http://'+request.host)
		lastmod = Time.now.strftime("%Y-%m-%d")
		xml.lastmod(lastmod)
		xml.changefreq("daily")
		xml.priority(1)
	}

	xml.url {
		xml.loc('http://'+request.host+'/news')
		lastmod = Time.now.strftime("%Y-%m-%d")
		xml.lastmod(lastmod)
		xml.changefreq("daily")
		xml.priority(1)
	}

	news = News.all
	news.each do |n| 
		xml.url {
			xml.loc('http://'+request.host+'/news/'+n.id.to_s)
			lastmod = Time.now.strftime("%Y-%m-%d")
			xml.lastmod(lastmod)
			xml.changefreq("daily")
			xml.priority(1)
		}
	end	

	xml.url {
		xml.loc('http://'+request.host+'/works')
		lastmod = Time.now.strftime("%Y-%m-%d")
		xml.lastmod(lastmod)
		xml.changefreq("daily")
		xml.priority(1)
	}

	xml.url {
		xml.loc('http://'+request.host+'/articles')
		lastmod = Time.now.strftime("%Y-%m-%d")
		xml.lastmod(lastmod)
		xml.changefreq("daily")
		xml.priority(1)
	}

	articles = Articles.all
	articles.each do |a| 
		xml.url {
			xml.loc('http://'+request.host+'/articles/'+a.id.to_s)
			lastmod = Time.now.strftime("%Y-%m-%d")
			xml.lastmod(lastmod)
			xml.changefreq("daily")
			xml.priority(1)
		}
	end	

	textpages = Textpage.all
	textpages.each do |t|
		xml.url {
			xml.loc('http://'+request.host+'/'+t.alias)
			lastmod = Time.now.strftime("%Y-%m-%d")
			xml.lastmod(lastmod)
			xml.changefreq("daily")
			xml.priority(1)
		}
	end
}