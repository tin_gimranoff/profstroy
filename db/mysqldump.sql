-- MySQL dump 10.13  Distrib 5.6.21, for osx10.9 (x86_64)
--
-- Host: localhost    Database: profstroy_development
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `active_admin_comments`
--

DROP TABLE IF EXISTS `active_admin_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `active_admin_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namespace` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `resource_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `resource_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author_id` int(11) DEFAULT NULL,
  `author_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_active_admin_comments_on_namespace` (`namespace`),
  KEY `index_active_admin_comments_on_author_type_and_author_id` (`author_type`,`author_id`),
  KEY `index_active_admin_comments_on_resource_type_and_resource_id` (`resource_type`,`resource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `active_admin_comments`
--

LOCK TABLES `active_admin_comments` WRITE;
/*!40000 ALTER TABLE `active_admin_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `active_admin_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `add_texts`
--

DROP TABLE IF EXISTS `add_texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `add_texts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `txt` text COLLATE utf8_unicode_ci,
  `image_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_file_size` int(11) DEFAULT NULL,
  `image_updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `add_texts`
--

LOCK TABLES `add_texts` WRITE;
/*!40000 ALTER TABLE `add_texts` DISABLE KEYS */;
INSERT INTO `add_texts` VALUES (1,'main_text','Текст на главной','<img alt=\"\" src=\"/ckeditor_assets/pictures/1/content_main-image.png\" style=\"float: left; margin-right: 24px;\" />\r\nLorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsu',NULL,NULL,NULL,NULL,'2014-11-14 11:52:41','2014-11-14 11:54:25'),(2,'panorama','Панорама','','panorama.png','image/png',422868,'2014-11-14 12:00:20','2014-11-14 11:54:50','2014-11-14 12:00:20'),(3,'table_on_main','таблица на главной','					<table>\r\n						<tr>\r\n							<th>Текст</th>\r\n							<th colspan=\"5\">Таблица</th>\r\n						</tr>\r\n						<tr>\r\n							<td>Текст</td>\r\n							<td>Текст</td>\r\n							<td>Текст</td>\r\n							<td>Текст</td>\r\n							<td>Текст</td>\r\n							<td>Текст</td>\r\n						</tr>\r\n						<tr>\r\n							<td>Текст</td>\r\n							<td>Текст</td>\r\n							<td>Текст</td>\r\n							<td>Текст</td>\r\n							<td>Текст</td>\r\n							<td>Текст</td>\r\n						</tr>\r\n						<tr>\r\n							<td>Текст</td>\r\n							<td>Текст</td>\r\n							<td>Текст</td>\r\n							<td>Текст</td>\r\n							<td>Текст</td>\r\n							<td>Текст</td>\r\n						</tr>\r\n					</table>',NULL,NULL,NULL,NULL,'2014-11-14 11:55:30','2014-11-14 11:55:30'),(4,'footer_text','Текст в футере','Уважаемые клиенты! Расширились Ваши возможности получить ипотечный кредит для покупки домовладения в \"Смольном\"! Поселок получил аккредитацию в \"Сбербанке\". Ставка от 13%, срок до 30 лет, первый взнос от 15%. Рассчитать кредит и получить более подробную информацию вы можете получить у нашего кредитного менеджера в \"Сбербанке\" - Олиферович Ирины Сергеевны по тел.: 8-981-787-9745',NULL,NULL,NULL,NULL,'2014-11-14 12:01:57','2014-11-14 12:01:57');
/*!40000 ALTER TABLE `add_texts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albums`
--

DROP TABLE IF EXISTS `albums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `albums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_file_size` int(11) DEFAULT NULL,
  `image_updated_at` datetime DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `intro` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `albums`
--

LOCK TABLES `albums` WRITE;
/*!40000 ALTER TABLE `albums` DISABLE KEYS */;
INSERT INTO `albums` VALUES (1,'work-img.png','image/png',245790,'2014-11-14 14:30:12','Название название','<strong>Бюджет:</strong> 400 тыс. руб.<br />	\r\n								<strong>Сроки:</strong> 2 мес.<br />	\r\n								<strong>Описание:</strong> бла бла бла бла бла бла бла бла бла бла бла бла бла бла бла .<br />	','2014-11-14 14:30:13','2014-11-14 14:30:13'),(2,'work-img.png','image/png',245790,'2014-11-14 14:43:40','Название название2','<strong>Бюджет:</strong> 400 тыс. руб.<br />	\r\n								<strong>Сроки:</strong> 2 мес.<br />	\r\n								<strong>Описание:</strong> бла бла бла бла бла бла бла бла бла бла бла бла бла бла бла .<br />	','2014-11-14 14:43:41','2014-11-14 14:43:41');
/*!40000 ALTER TABLE `albums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ckeditor_assets`
--

DROP TABLE IF EXISTS `ckeditor_assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ckeditor_assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_file_size` int(11) DEFAULT NULL,
  `assetable_id` int(11) DEFAULT NULL,
  `assetable_type` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ckeditor_assetable_type` (`assetable_type`,`type`,`assetable_id`),
  KEY `idx_ckeditor_assetable` (`assetable_type`,`assetable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ckeditor_assets`
--

LOCK TABLES `ckeditor_assets` WRITE;
/*!40000 ALTER TABLE `ckeditor_assets` DISABLE KEYS */;
INSERT INTO `ckeditor_assets` VALUES (1,'main-image.png','image/png',120353,1,'User','Ckeditor::Picture',275,206,'2014-11-14 11:51:50','2014-11-14 11:51:50');
/*!40000 ALTER TABLE `ckeditor_assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_file_size` int(11) DEFAULT NULL,
  `image_updated_at` datetime DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `intro` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'news-image.png','image/png',104503,'2014-11-14 11:31:38','Альянс Renault-Nissan сделает следующее поколение своего бестселлера более презентабельным.','В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster.','В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster.','2014-11-14 11:31:39','2014-11-14 11:31:39'),(2,'news-image.png','image/png',104503,'2014-11-14 11:32:12','Альянс Renault-Nissan сделает следующее поколение своего бестселлера более презентабельным.','В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster.','В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster.','2014-11-14 11:32:13','2014-11-14 11:32:13'),(3,'news-image.png','image/png',104503,'2014-11-14 11:33:05','Альянс Renault-Nissan сделает следующее поколение своего бестселлера более презентабельным.','В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster.','В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster.В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster. В Сеть просочилась первая информация о том, когда нам стоит ждать смены генераций сверхпопулярного в России кроссовера Renault Duster.','2014-11-14 11:33:05','2014-11-14 11:33:05');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_file_size` int(11) DEFAULT NULL,
  `image_updated_at` datetime DEFAULT NULL,
  `intro` text COLLATE utf8_unicode_ci,
  `album_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photos`
--

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
INSERT INTO `photos` VALUES (1,'work-img.png','image/png',245790,'2014-11-14 14:30:42','<strong>Бюджет:</strong> 400 тыс. руб.<br />\r\n<strong>Сроки:</strong> 2 мес.<br />\r\n<strong>Описание:</strong> бла бла бла бла бла бла бла бла бла бла бла бла бла бла бла .',1,'2014-11-14 14:30:43','2014-11-14 14:31:30'),(2,'slide.png','image/png',568660,'2014-11-14 14:33:09','<strong>Бюджет:</strong> 400 тыс. руб.<br />	\r\n								<strong>Сроки:</strong> 2 мес.<br />	\r\n								<strong>Описание:</strong> бла бла бла бла бла бла бла бла бла бла бла бла бла бла бла .<br />	',1,'2014-11-14 14:33:09','2014-11-14 14:33:09');
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20141114091338'),('20141114091351'),('20141114121545'),('20141114121546'),('20141114121547'),('20141114121548'),('20141114132438'),('20141114141536'),('20141114142326');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sliders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_file_size` int(11) DEFAULT NULL,
  `image_updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sliders`
--

LOCK TABLES `sliders` WRITE;
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
INSERT INTO `sliders` VALUES (1,'slide.png','image/png',568660,'2014-11-14 10:28:48','2014-11-14 10:28:49','2014-11-14 10:28:49'),(2,'slide.png','image/png',568660,'2014-11-14 10:34:33','2014-11-14 10:34:34','2014-11-14 10:34:34'),(3,'slide.png','image/png',568660,'2014-11-14 10:34:46','2014-11-14 10:34:47','2014-11-14 10:34:47');
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `textpages`
--

DROP TABLE IF EXISTS `textpages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `textpages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `textpages`
--

LOCK TABLES `textpages` WRITE;
/*!40000 ALTER TABLE `textpages` DISABLE KEYS */;
INSERT INTO `textpages` VALUES (1,'О КОМПАНИИ','about','Lorem','2014-11-14 13:31:32','2014-11-14 13:31:32'),(2,'ВАКАНСИИ','vacancys','Lorem','2014-11-14 13:32:02','2014-11-14 13:32:02'),(3,'ПОЛЕЗНО ЗНАТЬ','advice','Lorem','2014-11-14 13:32:39','2014-11-14 13:32:39'),(4,'СТРОИТЕЛЬСТВО','build','Lorem','2014-11-14 13:33:17','2014-11-14 13:33:17'),(5,'ОТДЕЛКА И РЕМОНТ','repair','Lorem','2014-11-14 13:35:33','2014-11-14 13:35:33'),(6,'САНТИХНИКА','sanitary','Lorem','2014-11-14 13:36:22','2014-11-14 13:36:22'),(7,'ВОДОСНАБЖЕНИЕ И ВОДООЧИСТКА','water','Lorem','2014-11-14 13:37:05','2014-11-14 13:37:05'),(8,'ВЕНТИЛЯЦИЯ','ventilation','Lorem','2014-11-14 13:37:54','2014-11-14 13:37:54'),(9,'ЭЛЕКТРИКА','electric','Lorem','2014-11-14 13:38:16','2014-11-14 13:38:16');
/*!40000 ALTER TABLE `textpages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) NOT NULL DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_email` (`email`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'gimranov.valentin@yandex.ru','$2a$10$JjIhlpuKO2j3JE1nWEW7YOPIzkxR5Oc6rGn7QDpJYuKQSNHUvGe6O',NULL,NULL,'2014-11-14 09:30:28',3,'2014-12-03 07:50:27','2014-11-14 09:30:28','127.0.0.1','127.0.0.1','2014-11-14 09:19:26','2014-12-03 07:50:27');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-05 15:19:41
