class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.has_attached_file :image
      t.text :intro
      t.belongs_to :album
      t.timestamps
    end
  end
end
