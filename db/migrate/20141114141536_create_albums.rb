class CreateAlbums < ActiveRecord::Migration
  def change
    create_table :albums do |t|
      t.has_attached_file :image
      t.string :title
      t.text :intro

      t.timestamps
    end
  end
end
