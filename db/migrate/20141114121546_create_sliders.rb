class CreateSliders < ActiveRecord::Migration
  def change
    create_table :sliders do |t|
      t.has_attached_file :image	

      t.timestamps
    end
  end
end
