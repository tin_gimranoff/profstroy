class CreateAddTexts < ActiveRecord::Migration
  def change
    create_table :add_texts do |t|
      t.string :label
      t.string :description
      t.text :txt
      t.has_attached_file :image

      t.timestamps
    end
  end
end
